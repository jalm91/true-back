const express = require('express');

const router = express.Router();
const { tasksController } = require('../controllers');

router.route('/').get(tasksController.generateTasks);
router.route('/getall').get(tasksController.getTasks);
router.route('/:uuid').put(tasksController.markTaskToDone);

module.exports = router;

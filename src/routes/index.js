const express = require('express');

const router = express.Router();
const healthRouter = require('./health.router');
const tasksRouter = require('./tasks.router');

router.use('/health', healthRouter);
router.use('/task', tasksRouter);


module.exports = router;

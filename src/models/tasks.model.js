const AWS = require("aws-sdk");
const { Error, Msg, config } = require('../constants/contants');
AWS.config.update({
    region: "us-east-1"
});

const docClient = new AWS.DynamoDB.DocumentClient();


async function getTasks() {
    let params = {
        TableName: config.taskTableName,
    };
    let { Items } = await docClient.scan(params).promise();
    return Items;
}

function saveTasks(Task) {

    const TaskToDynamo = Task.map(task => {
        const newTask = {
            PutRequest: {
                Item: task
            }
        }
        return newTask;
    })
    let params = {
        RequestItems: {
            [config.taskTableName]: TaskToDynamo
        },
    };

    docClient.batchWrite(params, function (err, data) {
        if (err) {
            console.error(Error.ErrorWritingBatch, JSON.stringify(err, null, 2));
            return false;
        } else {
            console.log(Msg.addingBatch, JSON.stringify(data, null, 2));
            return true;
        }
    });
    return true;
}


async function updateTask(uuid) {

    let params = {
        TableName: config.taskTableName,
        Key: {
            uuid
        },
        UpdateExpression: "set done = :doneValue",
        ExpressionAttributeValues: {
            ":doneValue": true
        },
        ReturnValues: "UPDATED_NEW"
    };

    docClient.update(params, function (err, data) {
        if (err) {
            console.error(Error.ErrorUpdatatingTask, JSON.stringify(err, null, 2));
        } else {
            console.log(Msg.UpdateTaskSuccess, JSON.stringify(data, null, 2));
        }
    });
}

module.exports = {
    saveTasks,
    getTasks,
    updateTask
}
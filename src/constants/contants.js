module.exports = {
  EndpointTitle: {
    url: 'https://lorem-faker.vercel.app/',
    params: 'api?quantity='
  },
  Error: {
    ErrorGenerateTask: 'Error generating Tasks',
    FailToGenerateTask: 'Fail to generated task',
    ErrorUpdatatingTask: 'Unable to update Task. ',
    ErrorWritingBatch: 'Fail to saving the batch of tasks'
  },
  Msg: {
    UpdateTaskSuccess: 'UpdateItem succeeded:',
    addingBatch: 'Batch of task added succed:',
  },
  config:{
    taskTableName: 'tasks',
  }
}
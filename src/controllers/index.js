module.exports = {
  healthController: require('./health.controller'),
  tasksController: require('./tasks.controller'),
};

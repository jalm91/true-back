function status(req, res) {
    return res.json({
        message: 'ALIVE',
        uptime: process.uptime(),
    });
}

module.exports = { status };

const axios = require('axios');
const { v4: uuidv4 } = require('uuid');
const { tasksModel } = require('../models');
const { EndpointTitle, Error } = require('../constants/contants');

const NUM_NAMES = 4;
async function generateTasks(req, res) {
    try {
        const { data: namesFromAPI } = await getNames(NUM_NAMES);
        const tasks = createTaskWithUUID(namesFromAPI);
        tasksModel.saveTasks(tasks);
        if (tasks) {
            return res.status(200).json(tasks);
        } else {
            throw new Error(Error.FailToGenerateTask);
        }
    } catch (error) {
        console.log(`${Error.ErrorGenerateTask} ${error}`)
        return res.status(500).json(error)
    }
}

function createTaskWithUUID(namesFromAPI) {
    console.log(namesFromAPI)
    const tasks = namesFromAPI.map(name => {
        return { name, uuid: uuidv4(), done: false }
    })
    return tasks;
}

async function getNames(numNames) {
    return await axios.get(`${EndpointTitle.url}${EndpointTitle.params}${numNames}`);
}

async function markTaskToDone(req, res) {
    const uuid = req.params.uuid;
    const resp = await tasksModel.updateTask(uuid);
    return res.status(200).json(resp);
}
async function getTasks(req, res) {
    const tasks = await tasksModel.getTasks();
    return res.status(200).json(tasks);
}

module.exports = {
    markTaskToDone,
    generateTasks,
    getTasks
}
const express = require('express');
const helmet = require('helmet');
const noCache = require('nocache');
const compression = require('compression');
const router = require('./src/routes');
const bodyParser = require('body-parser')


const app = express();
app.use(express.json());
app.use(compression());
app.use(helmet());
app.use(helmet.hsts({
  maxAge: 5184000,
}));
app.use(helmet.noSniff());
app.use(helmet.xssFilter());
app.use(helmet.permittedCrossDomainPolicies());
app.use(noCache());
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', router);


module.exports = app;

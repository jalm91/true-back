const { PORT, LOCAL } = process.env;

function launchServer() {
  console.log(`🚀 Controller starting on: http://localhost:${PORT}`);
}

(async () => {
  try {
    const app = require('./app');
    app.listen(PORT, launchServer);
  } catch (error) {
    console.log(`📢[Error starting server] ${error}`);
  }
})();

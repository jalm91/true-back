# Test to develop backend

## About
This part of the test to develop position
This applications expose an API with 3 endpoints

* Method GET localhost:4000/task/, It allow Us to get a list of task and save on dynamodb.
* Mehod GET  localhost:4000/task/getall, It allow Us to get all the task on task table.
* Method PUT localhost:4000/task/:uuid Allow us to completed a selected task

## Before Execution
* Be sure to set enviroment variables for AWS on Docker file.
* Create a table on dynamo db called `` task `` with id Primary partition key	called uuid type string.
* Create a image with the follow command `` docker build --tag backend:0 . ``

## Execution
The execution of the cli requires node >= 14.X.X, and it is run as shown next:

```$ docker run -p 4000:4000 -d backend:0```
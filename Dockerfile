FROM node:alpine
WORKDIR /home/node/app
COPY package.json /home/node/app
RUN npm install
COPY . /home/node/app
ENV PORT 4000
ENV AWS_ACCESS_KEY_ID ""
ENV AWS_SECRET_ACCESS_KEY ""
ENV "AWS_DEFAULT_REGION" "us-east-1"
EXPOSE 4000
# Run it
ENTRYPOINT ["node", "server.js"]